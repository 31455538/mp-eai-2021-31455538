# EAI: 2021
# Nombre del Alumno: Machaca, Aldo Martin
# DNI: 31455538

import os

def presionarTecla():
    input('Presione una tecla para continuar...')
    os.system('cls')

def menu():
    print('Gestion de empleados')
    print('1) Modificar empleado')
    print('2) Mostrar empleados en intervalo de...')
    print('3) Eliminar un empleado')
    print('4) Ordenar empleados de manera ascendente')
    print('5) Salir')
    op=int(input('Seleccione una opcion: '))
    while not (op >= 1) and (op <= 5):
        op=int(input('Seleccione una opcion: '))
    os.system('cls')
    return op

def leerId():
    id = int(input('Ingrese Id: '))
    return id

def validarId(empleados,id):
    validado = True
    for dato in empleados:
        if id == dato[0]:
            validado = False
            break
    return validado

def modificarNombre():
    nuevoNombre=input('Ingrese nombre: ')
    while not (nuevoNombre!=''):
        nuevoNombre=input('Ingrese nombre: ')
    return nuevoNombre

def modificarEmail():
    nuevoEmail=input('Ingrese email: ')
    while not (nuevoEmail!=''):
        nuevoEmail=input('Ingrese email: ')
    return nuevoEmail

def modificarDep():
    nuevoDep=input('Ingrese departamento: ')
    while not (nuevoDep!=''):
        nuevoDep=input('Ingrese departamento: ')
    return nuevoDep

def modificarEmpleados():
    print('Modificar empleado')
    continuar='s'
    while continuar=='s':
        id=leerId()
        if validarId(empleados,id):
            nombre=modificarNombre()
            email=modificarEmail()
            departamento=modificarDep()
            print('Empleado modificado')
        else:
            print('Id no existe')
        continuar=input('Desea modificar otro empleado? s/n: ')
    return empleados

def cargarEmpleados(empleados):
    #print('Carga de empleados...')
    empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]
            ]
    
def mostrarEmpleados(empleados):
    salarioMin=int(input('Ingrese salario minimo: '))
    salarioMax=int(input('Ingrese salario maximo: '))
    for datos in empleados:
        if (datos[4] >= salarioMin) and (datos[4] <= salarioMax):
            print(empleados)

'''def eliminarEmpleado(empleados,id):
    idAux=int(input('Ingrese id del empleado a eliminar: '))
    if validarId(empleados,idAux):'''


# A continuación un ejemplo que muestra la lista. Puede borrarlo            
'''for e in empleados:    
    print(e)'''


#Principal
os.system('cls')
op=0
empleados=[]

while op != 5:
    op=menu()
    cargarEmpleados(empleados)
    if op == 1:
        empleados=modificarEmpleados()
    elif op == 2:
        mostrarEmpleados(empleados)
    elif op ==3:
        eliminarEmpleado(empleados,id)
    elif op == 4:
        print('ordenar')
    elif op ==5:
        print('Salir')